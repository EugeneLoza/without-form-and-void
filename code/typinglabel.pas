unit TypingLabel;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleControls;

type
  TTypingLabel = class(TCastleLabel)
  private
    FTimer: Single;
    FTypingSpeed: Single;
    function TextLength: Integer;
  public
    procedure ResetText;
    procedure ShowAllText;
    function FinishedTyping: Boolean;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  published
    property TypingSpeed: Single read FTypingSpeed write FTypingSpeed default 60; //chars per second
  end;

implementation
uses
  CastleComponentSerialize;

procedure TTypingLabel.Update(const SecondsPassed: Single; var HandleInput: Boolean);
var
  N: Integer;
begin
  inherited;
  FTimer += SecondsPassed;
  N := Round(FTimer * TypingSpeed);
  if N < TextLength then
    MaxDisplayChars := N
  else
    MaxDisplayChars := -1; //show all
end;

function TTypingLabel.TextLength: Integer;
var
  S: String;
begin
  //TODO: Cache it!
  Result := 0;
  for S in Text do
    Result += Length(S);
end;

procedure TTypingLabel.ResetText;
begin
  FTimer := 0;
end;

procedure TTypingLabel.ShowAllText;
begin
  FTimer := 999;
end;

function TTypingLabel.FinishedTyping: Boolean;
begin
  Result := Round(FTimer * TypingSpeed) >= TextLength;
end;

constructor TTypingLabel.Create(AOwner: TComponent);
begin
  inherited;
  FTypingSpeed := 60;
  ResetText;
end;

initialization
  RegisterSerializableComponent(TTypingLabel, 'Typing Label');

end.

