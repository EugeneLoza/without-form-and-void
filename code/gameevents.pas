unit GameEvents;

{$mode objfpc}{$H+}

interface

uses
  GameContext, DOM;

type
  TMaxMin = object
    Max, Min: Integer;
    function RandomValue: Integer;
    procedure Validate;
  end;

type
  TGameEvent = class(TObject)
  public
    Id: String;
    Sanity, Concentration, Awareness: TMaxMin;
    Context: TContext;
    Text: String;
    procedure Validate;
    procedure Read(const Element: TDOMElement);
    class function RandomEvent: TGameEvent;
    constructor Create;
    destructor Destroy; override;
  end;

procedure LoadStory;
function GetRandomEvent(const Sanity, Concentration, Awareness: Integer; const AContext: TContext): String;
function GetEventByName(const AName: String): TGameEvent;

implementation
uses
  SysUtils, Classes, Generics.Collections,
  CastleXMLUtils, CastleLog,
  GameRandom;

type
  TEventsDictionary = specialize TObjectDictionary<String, TGameEvent>;

var
  EventDictionary: TEventsDictionary;
  AvailableEvents: TStringList; //reusable
  LastEventId: String = '';

function GetEventByName(const AName: String): TGameEvent;
begin
  Result := EventDictionary[AName];
end;

function GetRandomEvent(const Sanity, Concentration, Awareness: Integer; const AContext: TContext): String;

  function ValueIn(const AMaxMin: TMaxMin; const AValue: Integer): Boolean;
  begin
    if AMaxMin.Max * AValue >= 0 then
    begin
      if Abs(AValue) >= Abs(AMaxMin.Max) then
        Result := true
      else
        Result := false;
    end else
      Result := false;
  end;

var
  Event: TGameEvent;
begin
  AvailableEvents.Clear;
  for Event in EventDictionary.Values do
    if ValueIn(Event.Sanity, Sanity) and ValueIn(Event.Concentration, Concentration) and
      ValueIn(Event.Awareness, Awareness) and Event.Context.Convolution(AContext) then
        AvailableEvents.Add(Event.Id);
  if AvailableEvents.Count = 0 then
    raise Exception.Create('No event that corresponds to the requested context!');
  Result := AvailableEvents[Rnd.Random(AvailableEvents.Count)];
  if AvailableEvents.Count > 1 then
    while Result = LastEventId do
      Result := AvailableEvents[Rnd.Random(AvailableEvents.Count)];
  LastEventId := Result;
end;

procedure LoadStory;
var
  Event: TGameEvent;
  Doc: TXMLDocument;
  Iterator: TXMLElementIterator;
  Sa, Co, Aw: Integer;
  J: Integer;
begin
  EventDictionary := TEventsDictionary.Create([doOwnsValues]);

  Sa := 0;
  Co := 0;
  Aw := 0;
  Doc := URLReadXML('castle-data:/story/events.xml');
  try
    Iterator := Doc.DocumentElement.ChildrenIterator('event');
    try
      while Iterator.GetNext do
      begin
        Event := TGameEvent.Create;
        Event.Read(Iterator.Current);
        Event.Validate;
        EventDictionary.Add(Event.Id, Event);
        Sa += Event.Sanity.Max + Event.Sanity.Min;
        Co += Event.Concentration.Max + Event.Concentration.Min;
        Aw += Event.Awareness.Max + Event.Awareness.Min;
      end;
    finally FreeAndNil(Iterator) end;
  finally FreeAndNil(Doc) end;

  for J := 0 to 200 do
  begin
    Event := TGameEvent.RandomEvent;
    Event.Validate;
    EventDictionary.Add(Event.Id, Event);
    Sa += Event.Sanity.Max + Event.Sanity.Min;
    Co += Event.Concentration.Max + Event.Concentration.Min;
    Aw += Event.Awareness.Max + Event.Awareness.Min;
  end;
  WriteLnLog('Sanity average = ' + (Sa / 2).ToString);
  WriteLnLog('Concentration average = ' + (Co / 2).ToString);
  WriteLnLog('Awareness average = ' + (Aw / 2).ToString);

  AvailableEvents := TStringList.Create;
  AvailableEvents.Capacity := EventDictionary.Count;
end;

function TMaxMin.RandomValue: Integer;
begin
  if (Max = 0) then //we trust Min = 0 as Validate guarantees
    Result := 0 //exactly zero
  else
    Result := Min + Round((Max - Min) * Rnd.Random);
end;

procedure TMaxMin.Validate;
begin
  if Abs(Max) > 100 then
    raise Exception.Create('Validation error: |Max| must be < 100.');
  if Max * Min < 0 then
    raise Exception.Create('Validation error: Max and Min must be of the same sign.');
  if Abs(Max) < Abs(Min) then
    raise Exception.Create('Validation error: Max value must be equal or greater than Min.');
end;

procedure TGameEvent.Read(const Element: TDOMElement);
var
  S: String;
begin
  Id := Element.AttributeStringDef('Id', 'GE_' + Rnd.Random32bit.ToString); //it may fail under some very rare conditions
  Sanity.Max := Element.AttributeIntegerDef('maxs', 0);
  Sanity.Min := Element.AttributeIntegerDef('mins', Sanity.Max);
  Concentration.Max := Element.AttributeIntegerDef('maxc', 0);
  Concentration.Min := Element.AttributeIntegerDef('minc', Concentration.Max);
  Awareness.Max := Element.AttributeIntegerDef('maxa', 0);
  Awareness.Min := Element.AttributeIntegerDef('mina', Awareness.Max);
  Text := Element.AttributeString('text');
  S := Element.AttributeStringDef('allow', '');
  if S <> '' then
    Context.ALLOW.Add(S);
  S := Element.AttributeStringDef('deny', '');
  if S <> '' then
    Context.DENY.Add(S);
  S := Element.AttributeStringDef('demand', '');
  if S <> '' then
    Context.DEMAND.Add(S);
end;

class function TGameEvent.RandomEvent: TGameEvent;
begin
  Result := TGameEvent.Create;
  Result.Id := 'RE_' + Rnd.Random32bit.ToString; //it may fail under some very rare conditions
  Result.Sanity.Max := Rnd.Random(61) - 35;
  Result.Sanity.Min := Result.Sanity.Max div 2;
  Result.Concentration.Max := Rnd.Random(61) - 35;
  Result.Concentration.Min := Result.Concentration.Max div 2;
  Result.Awareness.Max := Rnd.Random(61) - 35;
  Result.Awareness.Min := Result.Awareness.Max div 2;
  Result.Text := 'Random Void event.';
end;

procedure TGameEvent.Validate;
begin
  Sanity.Validate;
  Concentration.Validate;
  Awareness.Validate;
  Context.Validate;
end;
constructor TGameEvent.Create;
begin
  Context := TContext.Create;
end;
destructor TGameEvent.Destroy;
begin
  Context.Free;
  inherited;
end;

finalization
  EventDictionary.Free;
  AvailableEvents.Free;
end.

