unit GameScreenMainMenu;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  GameAbstractScreen;

type
  TScreenMainMenu = class(TAbstractScreen)
  strict private
    ButtonStart, ButtonContinue, ButtonAchievements, ButtonOptions,
      ButtonCredits, ButtonQuit: TCastleButton;
    procedure ClickStart(Sender: TObject);
    procedure ClickContinue(Sender: TObject);
    procedure ClickAchievements(Sender: TObject);
    procedure ClickOptions(Sender: TObject);
    procedure ClickCredits(Sender: TObject);
    procedure ClickQuit(Sender: TObject);
  public
    function Background: TCastleImage; override;
    procedure Load(const ADesign: TCastleDesign); override;
    procedure Show; override;
  end;

var
  ScreenMainMenu: TScreenMainMenu;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleLog,
  CastleApplicationProperties, CastleWindow, CastleSoundEngine, CastleConfig,
  GameBackgrounds, GamePlayer, GameLocation, GameMap,
  GameStateMain, GameScreenAchievements, GameScreenOptions, GameScreenCredits,
  GameScreenCutscene, GameScreenEpicscene,
  GameScreenEvent;

procedure TScreenMainMenu.Load(const ADesign: TCastleDesign);
begin
  inherited;

  ButtonStart := ADesign.FindRequiredComponent('ButtonStart') as TCastleButton;
  ButtonContinue := ADesign.FindRequiredComponent('ButtonContinue') as TCastleButton;
  ButtonAchievements := ADesign.FindRequiredComponent('ButtonAchievements') as TCastleButton;
  ButtonOptions := ADesign.FindRequiredComponent('ButtonOptions') as TCastleButton;
  ButtonCredits := ADesign.FindRequiredComponent('ButtonCredits') as TCastleButton;
  ButtonQuit := ADesign.FindRequiredComponent('ButtonQuit') as TCastleButton;

  ButtonStart.OnClick := @ClickStart;
  ButtonContinue.OnClick := @ClickContinue;
  ButtonAchievements.OnClick := @ClickAchievements;
  ButtonOptions.OnClick := @ClickOptions;
  ButtonCredits.OnClick := @ClickCredits;
  ButtonQuit.OnClick := @ClickQuit;

  ButtonQuit.Exists := ApplicationProperties.ShowUserInterfaceToQuit;
  ButtonContinue.Enabled := false; //Save game exists
end;

function TScreenMainMenu.Background: TCastleImage;
begin
  Result := MenuBackground;
end;

procedure TScreenMainMenu.Show;
begin
  inherited;
  //SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('menu_music');
end;

procedure TScreenMainMenu.ClickStart(Sender: TObject);
begin
  ClearLocations;
  ClearMap;
  Player.Reset;
  ScreenCutscene.Caption := INTRO_CUTSCENE;
  ScreenCutscene.NextScreen := ScreenEpicscene;
  ScreenEpicscene.Caption := INTRO_EPICSCENE;
  ScreenEpicscene.NextScreen := ScreenEvent;
  StateMain.ChangeScreen(ScreenCutscene);
end;

procedure TScreenMainMenu.ClickContinue(Sender: TObject);
begin
  //Load save game
end;

procedure TScreenMainMenu.ClickAchievements(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenAchievements);
end;

procedure TScreenMainMenu.ClickOptions(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenOptions);
end;

procedure TScreenMainMenu.ClickCredits(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenCredits);
end;

procedure TScreenMainMenu.ClickQuit(Sender: TObject);
begin
  Application.MainWindow.Close;
end;

end.

