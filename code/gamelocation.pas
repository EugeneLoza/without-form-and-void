unit GameLocation;

{$mode objfpc}{$H+}

interface

uses
  GameEvents, GameContext;

type
  TLocation = class(TObject)
  strict private
    FEvent: String;
  public
    Id: String;
    Sanity, Concentration, Awareness: Integer;
    function Event(const AContext: TContext): TGameEvent;
  end;

function NewRandomLocation: TLocation;
function GetLocationByName(const ALocationName: String): TLocation;
procedure ClearLocations;

implementation
uses
  SysUtils, Generics.Collections,
  GameRandom;

type
  TLocationsList = specialize TObjectDictionary<String, TLocation>;

var
  LocationsList: TLocationsList;
  N: Integer;
  //global statistics:
  GlobalSanity, GlobalConcentration, GlobalAwareness: Integer;

procedure ClearLocations;
begin
  if LocationsList = nil then
    LocationsList := TLocationsList.Create([doOwnsValues])
  else
    LocationsList.Clear;
  N := 0;
  GlobalSanity := 0;
  GlobalConcentration := 0;
  GlobalAwareness := 0;
end;

procedure AddLocation(const ALocation: TLocation);
begin
  LocationsList.Add(ALocation.Id, ALocation);
  GlobalSanity += ALocation.Sanity;
  GlobalConcentration += ALocation.Concentration;
  GlobalAwareness += ALocation.Awareness;
end;

function NewRandomLocation: TLocation;
const
  Span = 70;
var
  Total: Integer;
begin
  Result := TLocation.Create;
  Result.Id := 'GL_' + Rnd.Random32bit.ToString; //it may fail under some very rare conditions
  repeat
    Result.Sanity := Rnd.Random(Span * 2 + 1) - Span;
    Result.Concentration := Rnd.Random(Span * 2 + 1) - Span;
    Result.Awareness := Rnd.Random(Span * 2 + 1) - Span;
    Total := 0;
    Total += Abs(Result.Sanity);
    Total += Abs(Result.Concentration);
    Total += Abs(Result.Awareness);
  until Total > 30;
  AddLocation(Result);
end;

function GetLocationByName(const ALocationName: String): TLocation;
begin
  Result := LocationsList[ALocationName];
end;

function TLocation.Event(const AContext: TContext): TGameEvent;
begin
  if FEvent = '' then
    FEvent := GetRandomEvent(Sanity, Concentration, Awareness, AContext);
  Result := GetEventByName(FEvent);
end;

finalization
  FreeAndNil(LocationsList);

end.

