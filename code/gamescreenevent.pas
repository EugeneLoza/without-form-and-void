unit GameScreenEvent;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  TypingLabel,
  GameAbstractScreen;

const
  DailySanityBonus = 10;
  DailyConcentrationBonus = 10;
  DailyAwarenessBonus = 10;

type
  TScreenEvent = class(TAbstractScreen)
  strict private
    EventText: TTypingLabel;
    SanityBackground, ConcenrationBackground, AwarenessBackground: TCastleImageControl;
    SanityValueLabel, ConcentrationValueLabel, AwarenessValueLabel: TCastleLabel;
    procedure ClickNext(Sender: TObject);
    function RewardToString(const AReward: Integer): String;
  public
    function Background: TCastleImage; override;
    procedure Show; override;
    function ShowPlayerUi: Boolean; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenEvent: TScreenEvent;

implementation
uses
  SysUtils,
  CastleComponentSerialize, CastleLog,
  GameBackgrounds, GamePlayer,
  GameLocation, GameEvents, GameContext,
  GameStateMain, GameScreenMainMenu, GameScreenCutscene, GameScreenEpicscene,
  GameScreenMap;

function TScreenEvent.RewardToString(const AReward: Integer): String;
begin
  if AReward >= 0 then
    Result := '+' + IntToStr(AReward)
  else
    Result := IntToStr(AReward);
end;

procedure TScreenEvent.Show;
var
  Location: TLocation;
  Context: TContext;
  Event: TGameEvent;
  SanityReward, ConcentrationReward, AwarenessReward: Integer;
begin
  inherited;
  Location := NewRandomLocation;

  Context := TContext.Create;
  if Player.Depth < 7 then
    Context.DEMAND.Add('BEGIN');

  Event := Location.Event(Context);
  SanityReward := Player.AddSanity(Event.Sanity.RandomValue);
  ConcentrationReward := Player.AddConcentration(Event.Concentration.RandomValue);
  AwarenessReward := Player.AddAwareness(Event.Awareness.RandomValue);

  Context.Free;

  EventText.Caption := Event.Text;
  EventText.ResetText;
  SanityBackground.Exists := SanityReward <> 0;
  SanityValueLabel.Caption := '<b>' + RewardToString(SanityReward) + '</b>';
  ConcenrationBackground.Exists := ConcentrationReward <> 0;
  ConcentrationValueLabel.Caption := '<b>' + RewardToString(ConcentrationReward) + '</b>';
  AwarenessBackground.Exists := AwarenessReward <> 0;
  AwarenessValueLabel.Caption := '<b>' + RewardToString(AwarenessReward) + '</b>';
end;

function TScreenEvent.ShowPlayerUi: Boolean;
begin
  Result := true;
end;

procedure TScreenEvent.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonNext') as TCastleButton).OnClick := @ClickNext;
  EventText := ADesign.FindRequiredComponent('TextLabel') as TTypingLabel;
  SanityBackground := ADesign.FindRequiredComponent('SanityBackground') as TCastleImageControl;
  ConcenrationBackground := ADesign.FindRequiredComponent('ConcenrationBackground') as TCastleImageControl;
  AwarenessBackground := ADesign.FindRequiredComponent('AwarenessBackground') as TCastleImageControl;
  SanityValueLabel := ADesign.FindRequiredComponent('SanityValueLabel') as TCastleLabel;
  ConcentrationValueLabel := ADesign.FindRequiredComponent('ConcentrationValueLabel') as TCastleLabel;
  AwarenessValueLabel := ADesign.FindRequiredComponent('AwarenessValueLabel') as TCastleLabel;
end;

function TScreenEvent.Background: TCastleImage;
begin
  Result := SubMenuBackground; //random background, random effect
end;

procedure TScreenEvent.ClickNext(Sender: TObject);
var
  DeathCount: Integer;
begin
  if EventText.FinishedTyping then
  begin
    if Player.IsAlive then
    begin
      Inc(Player.Depth);
      WriteLnLog(Player.Depth.ToString);
      if Player.Depth < 7 then
        StateMain.ChangeScreen(ScreenEvent)
      else
      if Player.Depth = 7 then
      begin
        ScreenCutscene.Caption := START_CUTSCENE;
        ScreenCutscene.NextScreen := ScreenEpicscene;
        ScreenEpicscene.NextScreen := ScreenEvent; //ScreenMap
        //Generate map position
        ScreenEpicscene.Caption := START_EPICSCENE;
        StateMain.ChangeScreen(ScreenCutscene);
      end else
      begin
        if (Player.Depth - 7) mod 8 = 0 then
        begin
          ScreenCutscene.Caption := Format(DAY_CUTSCENE, [
            IntToStr((Player.Depth - 7) div 8),
            RewardToString(Player.AddSanity(DailySanityBonus)),
            RewardToString(Player.AddConcentration(DailyConcentrationBonus)),
            RewardToString(Player.AddAwareness(DailyAwarenessBonus))
            ]);
          ScreenCutscene.NextScreen := ScreenMap;
          StateMain.ChangeScreen(ScreenCutscene);
        end else
          StateMain.ChangeScreen(ScreenMap);
      end;
    end else
    begin
      DeathCount := 0;
      if Player.Sanity = 0 then
      begin
        inc(DeathCount);
        ScreenEpicScene.Caption := DEATH_SANITY_EPICSENE;
      end;
      if Player.Concentration = 0 then
      begin
        inc(DeathCount);
        ScreenEpicScene.Caption := DEATH_CONCENTRATION_EPICSENE;
      end;
      if Player.Awareness = 0 then
      begin
        inc(DeathCount);
        ScreenEpicScene.Caption := DEATH_AWARENESS_EPICSENE;
      end;
      if DeathCount > 1 then
        ScreenEpicScene.Caption := DEATH_EPICSENE;
      ScreenEpicScene.NextScreen := ScreenMainMenu;
      StateMain.ChangeScreen(ScreenEpicScene);
    end;
  end else
    EventText.ShowAllText;
end;

end.

