unit GameMap;

{$mode objfpc}{$H+}

interface

uses
  Generics.Collections;

type
  TXY = object
    X, Y: Integer;
  end;

type
  TMap = specialize TDictionary<TXY, String>;

var
  Map: TMap;

procedure ClearMap;
implementation
uses
  SysUtils;

procedure ClearMap;
begin
  if Map = nil then
    Map := TMap.Create
  else
    Map.Clear;
end;

finalization;
  FreeAndNil(Map);
end.

