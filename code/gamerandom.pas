unit GameRandom;

{$mode objfpc}{$H+}

interface

uses
  CastleRandom;

var
  Rnd: TCastleRandom;

implementation

Initialization
  Rnd := TCastleRandom.Create;

finalization
  Rnd.Free;

end.

