unit GameScreenEpicscene;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  TypingLabel,
  GameAbstractScreen;

resourcestring
  INTRO_EPICSCENE =
    'This was the day I discovered the Void.';
  START_EPICSCENE =
    'This was the day I was lost in the Void.';
  DEATH_SANITY_EPICSENE =
    'As the last spark of sanity left my mind I became one with the Chaos without Form.';
  DEATH_CONCENTRATION_EPICSENE =
    'I could no longer resist the currents and they took me away into the Depths of Darkness.';
  DEATH_AWARENESS_EPICSENE =
    'I had lost the sense of direction and time, the margin between reality and dreams. Like many other I was forever lost in the Void.';
  DEATH_EPICSENE =
    'There was no more strength in me to resist. Nothing made sense any longer. I was forever lost in the Void.';

type
  TScreenEpicscene = class(TAbstractScreen)
  strict private
    CutsceneText, CutsceneShadow: TTypingLabel;
    procedure ClickNext(Sender: TObject);
  public
    Caption: String;
    NextScreen: TAbstractScreen;
    function Background: TCastleImage; override;
    procedure Show; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenEpicscene: TScreenEpicscene;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds,
  GameStateMain, GameScreenEvent;

procedure TScreenEpicscene.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonNext') as TCastleButton).OnClick := @ClickNext;
  CutsceneText := ADesign.FindRequiredComponent('CutsceneText') as TTypingLabel;
  CutsceneShadow := ADesign.FindRequiredComponent('CutsceneShadow') as TTypingLabel;
end;

procedure TScreenEpicscene.Show;
begin
  inherited;
  CutsceneText.Caption := '<b>' + Caption + '</b>';
  CutsceneShadow.Caption := '<b>' + Caption + '</b>';
  CutsceneText.ResetText;
  CutsceneShadow.ResetText;
end;

function TScreenEpicscene.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenEpicscene.ClickNext(Sender: TObject);
begin
  if CutsceneText.FinishedTyping then
    StateMain.ChangeScreen(NextScreen)
  else
  begin
    CutsceneText.ShowAllText;
    CutsceneShadow.ShowAllText;
  end;
end;

end.

