unit GameScreenAchievements;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  GameAbstractScreen;

type
  TScreenAchievements = class(TAbstractScreen)
  strict private
    procedure ClickBack(Sender: TObject);
  public
    function Background: TCastleImage; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenAchievements: TScreenAchievements;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds,
  GameStateMain, GameScreenMainMenu;

procedure TScreenAchievements.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonBack') as TCastleButton).OnClick := @ClickBack;
end;

function TScreenAchievements.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenAchievements.ClickBack(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenMainMenu);
end;

end.

