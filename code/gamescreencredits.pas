unit GameScreenCredits;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  GameAbstractScreen;

type
  TScreenCredits = class(TAbstractScreen)
  strict private
    procedure ClickBack(Sender: TObject);
  public
    function Background: TCastleImage; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenCredits: TScreenCredits;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds,
  GameStateMain, GameScreenMainMenu;

procedure TScreenCredits.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonBack') as TCastleButton).OnClick := @ClickBack;
end;

function TScreenCredits.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenCredits.ClickBack(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenMainMenu);
end;

end.

