unit GameScreenMap;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleUiControls, CastleImages,
  GameAbstractScreen;

type
  TScreenMap = class(TAbstractScreen)
  strict private
    NodeDescriptionLabel: TCastleLabel;
    ButtonExplore: TCastleButton;
    GroupMap: TCastleUserInterface;
    procedure ClickExplore(Sender: TObject);
  public
    function Background: TCastleImage; override;
    function ShowPlayerUi: Boolean; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenMap: TScreenMap;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds, GameMap,
  GameStateMain, GameScreenEvent;

procedure TScreenMap.Load(const ADesign: TCastleDesign);
begin
  inherited;
  ButtonExplore := ADesign.FindRequiredComponent('ButtonExplore') as TCastleButton;
  ButtonExplore.OnClick := @ClickExplore;
  NodeDescriptionLabel := ADesign.FindRequiredComponent('NodeDescriptionLabel') as TCastleLabel;
  GroupMap := ADesign.FindRequiredComponent('GroupMap') as TCastleUserInterface;
  //GroupMap.InsertFront(MapUi);
end;

function TScreenMap.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenMap.ClickExplore(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenEvent);
end;

function TScreenMap.ShowPlayerUi: Boolean;
begin
  Result := true;
end;

end.

