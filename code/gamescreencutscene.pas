unit GameScreenCutscene;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  TypingLabel,
  GameAbstractScreen;

resourcestring
  INTRO_CUTSCENE =
    'In the Beginning God created the Heaven and the Earth.' + '<br/>' +
    'And the Earth was Without Form and Void.' + '<br/>' +
    'And Darkness was upon the Face of the Deep.' + '<br/>' +
    'Primordial Chaos, of which all living beings and inanimate objects were born was hidden from humanity for a reason.' + '<br/>' +
    'Only few unfortunate souls have found a way to uncover the concealed Knowledge,' + '<br/>' +
    'to find their doom in the unfathomable Void that was there before the beginning of the Time.';
  START_CUTSCENE =
    'My curiosity was my downfall.' + '<br/>' +
    'My desire for knowledge was my undoing.' + '<br/>' +
    'When I failed to realize where I was,' + '<br/>' +
    'a stroke of fear came into my mind.';
  DAY_CUTSCENE =
    'Suddenly I had felt the Dawn come somewhere far away.' + '<br/>' +
    'There was Evening, and there was Morning - day %s.' + '<br/>' +
    '(%s Sanity, %s Concentration, %s Awareness)';

type
  TScreenCutscene = class(TAbstractScreen)
  strict private
    CutsceneText, CutsceneShadow: TTypingLabel;
    procedure ClickNext(Sender: TObject);
  public
    Caption: String;
    NextScreen: TAbstractScreen;
    function Background: TCastleImage; override;
    procedure Show; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenCutscene: TScreenCutscene;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds,
  GameStateMain, GameScreenEvent;

procedure TScreenCutscene.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonNext') as TCastleButton).OnClick := @ClickNext;
  CutsceneText := ADesign.FindRequiredComponent('CutsceneText') as TTypingLabel;
  CutsceneShadow := ADesign.FindRequiredComponent('CutsceneShadow') as TTypingLabel;
end;

procedure TScreenCutscene.Show;
begin
  inherited;
  CutsceneText.Caption := '<b>' + Caption + '</b>';
  CutsceneShadow.Caption := '<b>' + Caption + '</b>';
  CutsceneText.ResetText;
  CutsceneShadow.ResetText;
end;

function TScreenCutscene.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenCutscene.ClickNext(Sender: TObject);
begin
  if CutsceneText.FinishedTyping then
    StateMain.ChangeScreen(NextScreen)
  else
  begin
    CutsceneText.ShowAllText;
    CutsceneShadow.ShowAllText;
  end;
end;

end.

