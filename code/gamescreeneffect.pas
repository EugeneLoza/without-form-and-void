unit GameScreenEffect;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleScreenEffects, X3DNodes, X3DFields, X3DLoad, CastleTimeUtils,
  CastleImages, CastleColors;

type
  TScreenEffect = class(TCastleScreenEffects)
  strict private
    {BlurRoot: TX3DRootNode;
    BlurEffect: TScreenEffectNode;
    BlurStrengthUniform: TSFFloat; }
    FBlurEffectValue: Single;
    procedure SetBlurEffect(const AStrength: Single);
    procedure ClearScreenEffects;
  public
    {property BlurEffectValue: Single read FBlurEffectValue write SetBlurEffect;}
    procedure Load(const AImage: TCastleImage; const AColor: TCastleColor; const AOwns: Boolean);
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwenr: TComponent); override;
    destructor Destroy; override;
  end;

implementation
uses
  CastleControls;

procedure TScreenEffect.ClearScreenEffects;
begin
  FBlurEffectValue := -1;
  SetBlurEffect(0.0);
end;

procedure TScreenEffect.SetBlurEffect(const AStrength: Single);
const
  Epsilon = 1e-4;
begin
  {if FBlurEffectValue <> AStrength then
  begin
    FBlurEffectValue := AStrength;
    BlurEffect.Enabled := AStrength > Epsilon;
    BlurStrengthUniform.Send(AStrength);
  end;}
end;

procedure TScreenEffect.Load(const AImage: TCastleImage; const AColor: TCastleColor; const AOwns: Boolean);
var
  Img: TCastleImageControl;
begin
  Img := TCastleImageControl.Create(Self);
  Img.OwnsImage := AOwns;
  Img.FullSize := true;
  Img.Image := AImage;
  Img.Color := AColor;
  InsertFront(Img);
end;

procedure TScreenEffect.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;

  {if BlurEffect.Enabled then
    BlurEffectValue := BlurEffectValue + SecondsPassed;}
end;

constructor TScreenEffect.Create(AOwenr: TComponent);
begin
  inherited;
  FullSize := true;

  {BlurRoot := LoadNode('castle-data:/screen_effects/fpp/blur.x3dv');
  BlurEffect := BlurRoot.FindNode('MyScreenEffect') as TScreenEffectNode;
  BlurStrengthUniform := BlurEffect.FindNode('MyShader').Field('EffectStrength', true) as TSFFloat;
  AddScreenEffect(BlurEffect); }
  ClearScreenEffects;
end;

destructor TScreenEffect.Destroy;
begin
  //FreeAndNil(BlurRoot);
  inherited;
end;

end.

