unit GameInitialize;

interface

implementation

uses
  CastleWindow, CastleLog, CastleKeysMouse, CastleConfig, CastleSoundEngine,
  CastleApplicationProperties, CastleUIState,
  GameBackgrounds, GameEvents,
  GameStateMain, GameScreenMainMenu, GameScreenAchievements, GameScreenOptions,
  GameScreenCredits, GameScreenEvent, GameScreenCutscene, GameScreenEpicscene,
  GameScreenMap;

var
  Window: TCastleWindowBase;

procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');

  UserConfig.Load;
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';
  SoundEngine.Volume := UserConfig.GetFloat('volume', 1.0);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music', 1.0);

  LoadBackgrounds;
  LoadStory;

  StateMain := TStateMain.Create(Application);
  ScreenMainMenu := TScreenMainMenu.Create(StateMain);
  ScreenAchievements := TScreenAchievements.Create(StateMain);
  ScreenOptions := TScreenOptions.Create(StateMain);
  ScreenCredits := TScreenCredits.Create(StateMain);
  ScreenEvent := TScreenEvent.Create(StateMain);
  ScreenCutscene := TScreenCutscene.Create(StateMain);
  ScreenEpicscene := TScreenEpicscene.Create(StateMain);
  ScreenMap := TScreenMap.Create(StateMain);
  TUIState.Current := StateMain;
end;

initialization
  ApplicationProperties.ApplicationName := 'void';

  if IsLibrary then
    InitializeLog;

  {
  Theme.LoadingBackgroundColor := HexToColor('6fbee4');
  Theme.Images[tiLoading] := SplashImage;
  Theme.OwnsImages[tiLoading] := false;
  Theme.LoadingImageForWindowHeight := 1334;
  }

  Application.OnInitialize := @ApplicationInitialize;

  Window := TCastleWindowBase.Create(Application);
  Window.Caption := 'Without Form and Void';
  Application.MainWindow := Window;

  {$ifndef CASTLE_IOS}
    {$ifndef ANDROID}
      Window.Height := Application.ScreenHeight * 5 div 6;
      Window.Width := Window.Height * 1920 div 1080;
    {$endif}
  {$endif}
end.
