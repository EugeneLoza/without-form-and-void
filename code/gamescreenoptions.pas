unit GameScreenOptions;

{$mode objfpc}{$H+}

interface

uses
  CastleControls, CastleImages,
  GameAbstractScreen;

type
  TScreenOptions = class(TAbstractScreen)
  strict private
    procedure ClickBack(Sender: TObject);
  public
    function Background: TCastleImage; override;
    procedure Load(const ADesign: TCastleDesign); override;
  end;

var
  ScreenOptions: TScreenOptions;

implementation
uses
  CastleComponentSerialize,
  GameBackgrounds,
  GameStateMain, GameScreenMainMenu;

procedure TScreenOptions.Load(const ADesign: TCastleDesign);
begin
  inherited;
  (ADesign.FindRequiredComponent('ButtonBack') as TCastleButton).OnClick := @ClickBack;
end;

function TScreenOptions.Background: TCastleImage;
begin
  Result := SubMenuBackground;
end;

procedure TScreenOptions.ClickBack(Sender: TObject);
begin
  StateMain.ChangeScreen(ScreenMainMenu);
end;

end.

