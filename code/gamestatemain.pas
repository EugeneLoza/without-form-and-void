unit GameStateMain;

interface

uses
  Classes,
  CastleUIState, CastleUIControls, CastleControls,
  CastleKeysMouse,
  GameScreenEffect, GamePlayer,
  GameAbstractScreen;

type
  TStateMain = class(TUIState)
  strict private
    PlayerUi: TCastleUserInterface;
    SanityLabel, ConcentrationLabel, AwarenessLabel: TCastleLabel;
    Root: TCastleUserInterface;
    NewScreenEffect, OldScreenEffect: TScreenEffect;
    CurrentScreen: TAbstractScreen;
  public
    procedure ChangeScreen(const AScreen: TAbstractScreen);
  public
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    constructor Create(AOwenr: TComponent); override;
    destructor Destroy; override;
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils,
  CastleComponentSerialize,
  CastleColors,
  GameScreenMainMenu, GameScreenAchievements, GameScreenOptions, GameScreenCredits,
  GameScreenEvent, GameScreenCutscene, GameScreenEpicScene, GameScreenMap;

procedure TStateMain.Start;
var
  UiOwner: TComponent;
begin
  inherited;
  InsertUserInterface('castle-data:/state_main.castle-user-interface', FreeAtStop, UiOwner);

  Root := UiOwner.FindRequiredComponent('Root') as TCastleUserInterface;
  PlayerUi := UiOwner.FindRequiredComponent('PlayerUi') as TCastleUserInterface;
  SanityLabel := UiOwner.FindRequiredComponent('SanityLabel') as TCastleLabel;
  ConcentrationLabel := UiOwner.FindRequiredComponent('ConcentrationLabel') as TCastleLabel;
  AwarenessLabel := UiOwner.FindRequiredComponent('AwarenessLabel') as TCastleLabel;

  ScreenMainMenu.Load(UiOwner.FindRequiredComponent('ScreenMainMenu') as TCastleDesign);
  ScreenMainMenu.Hide;
  ScreenAchievements.Load(UiOwner.FindRequiredComponent('ScreenAchievements') as TCastleDesign);
  ScreenAchievements.Hide;
  ScreenOptions.Load(UiOwner.FindRequiredComponent('ScreenOptions') as TCastleDesign);
  ScreenOptions.Hide;
  ScreenCredits.Load(UiOwner.FindRequiredComponent('ScreenCredits') as TCastleDesign);
  ScreenCredits.Hide;
  ScreenEvent.Load(UiOwner.FindRequiredComponent('ScreenEvent') as TCastleDesign);
  ScreenEvent.Hide;
  ScreenMap.Load(UiOwner.FindRequiredComponent('ScreenMap') as TCastleDesign);
  ScreenMap.Hide;
  ScreenCutscene.Load(UiOwner.FindRequiredComponent('ScreenCutscene') as TCastleDesign);
  ScreenCutscene.Hide;
  ScreenEpicscene.Load(UiOwner.FindRequiredComponent('ScreenEpicscene') as TCastleDesign);
  ScreenEpicscene.Hide;

  CurrentScreen := ScreenMainMenu;
  NewScreenEffect := TScreenEffect.Create(Nil);
  NewScreenEffect.Load(CurrentScreen.Background, White, false);
  InsertControl(0, NewScreenEffect);
  CurrentScreen.Show;
  PlayerUi.Exists := false;
end;

procedure TStateMain.ChangeScreen(const AScreen: TAbstractScreen);
begin
  if OldScreenEffect <> nil then
  begin
    RemoveControl(OldScreenEffect);
    OldScreenEffect.Free; //to avoid old effects being stacked in memory and freed only after TStateMain(Self) frees
  end;
  Assert(NewScreenEffect <> nil);
  OldScreenEffect := NewScreenEffect;
  OldScreenEffect.Load(Container.SaveScreen, White, true); //old effect is now bottom

  NewScreenEffect := TScreenEffect.Create(nil);
  NewScreenEffect.Load(AScreen.Background, AScreen.Color, false);
  InsertControl(1, NewScreenEffect); //new effect is now second-from-bottom

  PlayerUi.Exists := false;
  CurrentScreen.Hide;
  CurrentScreen := AScreen;
  CurrentScreen.Show;
  PlayerUi.Exists := CurrentScreen.ShowPlayerUi;
end;

procedure TStateMain.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  CurrentScreen.Update;
  Player.Update;
  SanityLabel.Caption := 'Sanity: ' + IntToStr(Player.UiSanity) + '/' + IntToStr(Player.MaxSanity);
  ConcentrationLabel.Caption := 'Concentration: ' + IntToStr(Player.UiConcentration) + '/' + IntToStr(Player.MaxConcentration);
  AwarenessLabel.Caption := 'Awareness: ' + IntToStr(Player.UiAwareness) + '/' + IntToStr(Player.MaxAwareness);
end;

constructor TStateMain.Create(AOwenr: TComponent);
begin
  Player := TPlayer.Create;
  Player.Reset;
  inherited;
end;

destructor TStateMain.Destroy;
begin
  Player.Free;
  FreeAndNil(NewScreenEffect);
  FreeAndNil(OldScreenEffect);
  inherited;
end;

end.
