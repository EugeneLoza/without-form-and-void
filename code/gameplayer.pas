unit GamePlayer;

{$mode objfpc}{$H+}

interface

type
  TPlayer = class(TObject)
  public
    Sanity, Concentration, Awareness: Integer;
    MaxSanity, MaxConcentration, MaxAwareness: Integer;
    UiSanity, UiConcentration, UiAwareness: Integer;
    Depth: Integer;
    function AddSanity(const ADiff: Integer): Integer;
    function AddConcentration(const ADiff: Integer): Integer;
    function AddAwareness(const ADiff: Integer): Integer;
    function IsAlive: Boolean;
    procedure Reset;
    procedure Update;
  end;

var
  Player: TPlayer;

implementation

procedure TPlayer.Reset;
begin
  Depth := 0;
  MaxSanity := 100;
  Sanity := MaxSanity;
  UiSanity := Sanity;
  MaxConcentration := 100;
  Concentration := MaxConcentration;
  UiConcentration := Concentration;
  MaxAwareness := 100;
  Awareness := MaxAwareness;
  UiAwareness := Awareness;
end;

function TPlayer.AddSanity(const ADiff: Integer): Integer;
begin
  Result := ADiff;
  if ADiff >= 0 then
  begin
    Sanity += ADiff;
    if Sanity > 100 then
    begin
      Result := ADiff - (Sanity - 100);
      Sanity := 100;
    end;
  end else
  begin
    if Sanity + ADiff < 0 then
    begin
      Result := -Sanity;
      Sanity := 0;
    end else
      Sanity += ADiff;
  end;
end;

function TPlayer.AddConcentration(const ADiff: Integer): Integer;
begin
  Result := ADiff;
  if ADiff >= 0 then
  begin
    Concentration += ADiff;
    if Concentration > 100 then
    begin
      Result := ADiff - (Concentration - 100);
      Concentration := 100;
    end;
  end else
  begin
    if Concentration + ADiff < 0 then
    begin
      Result := -Concentration;
      Concentration := 0;
    end else
      Concentration += ADiff;
  end;
end;

function TPlayer.AddAwareness(const ADiff: Integer): Integer;
begin
  Result := ADiff;
  if ADiff >= 0 then
  begin
    Awareness += ADiff;
    if Awareness > 100 then
    begin
      Result := ADiff - (Awareness - 100);
      Awareness := 100;
    end;
  end else
  begin
    if Awareness + ADiff < 0 then
    begin
      Result := -Awareness;
      Awareness := 0;
    end else
      Awareness += ADiff;
  end;
end;

function TPlayer.IsAlive: Boolean;
begin
  Result := (Sanity > 0) and (Concentration > 0) and (Awareness > 0);
end;

procedure TPlayer.Update;

  function Sgn(const AInt: Integer): Integer;
  begin
    if AInt > 0 then
      Result := 1
    else
    if AInt < 0 then
      Result := -1
    else
      Result := 0;
  end;

begin
  UiSanity += Sgn(Sanity - UiSanity);
  UiConcentration += Sgn(Concentration - UiConcentration);
  UiAwareness += Sgn(Awareness - UiAwareness);
end;

end.

