unit GameAbstractScreen;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  CastleControls, CastleImages, CastleColors;

type
  TAbstractScreen = class abstract(TComponent)
  strict protected
    Root: TCastleDesign;
  public
    function Background: TCastleImage; virtual; abstract;
    function Color: TCastleColor; virtual;
    function ShowPlayerUi: Boolean; virtual;
    procedure Load(const ADesign: TCastleDesign); virtual;
    procedure Show; virtual;
    procedure Hide;
    procedure Update; virtual;
  end;

implementation
uses
  CastleComponentSerialize;

function TAbstractScreen.Color: TCastleColor;
begin
  Result := White;
end;

function TAbstractScreen.ShowPlayerUi: Boolean;
begin
  Result := false;
end;

procedure TAbstractScreen.Load(const ADesign: TCastleDesign);
begin
  Root := ADesign;
end;

procedure TAbstractScreen.Show;
begin
  Root.Exists := true;
  Update;
end;

procedure TAbstractScreen.Hide;
begin
  Root.Exists := false;
end;

procedure TAbstractScreen.Update;
begin
end;


end.

