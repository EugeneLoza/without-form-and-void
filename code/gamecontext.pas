unit GameContext;

{$mode objfpc}{$H+}

interface

uses
  Classes;

type
  TContext = class(TObject)
  public
    Allow, Demand, Deny: TStringList;
    function Convolution(const AContext: TContext): Boolean;
    procedure Validate;
    constructor Create;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils;

type
  TStringListHelper = class helper for TStringList
  public
    function Contains(const AString: String): Boolean;
  end;

function TStringListHelper.Contains(const AString: String): Boolean;
var
  S: String;
begin
  for S in Self do
    if S = AString then
      Exit(true);
  Result := false;
end;

function TContext.Convolution(const AContext: TContext): Boolean;
var
  S: String;
begin
  for S in Self.Demand do
    if not AContext.Allow.Contains(S) and not AContext.Demand.Contains(S) then
      Exit(false);
  for S in AContext.Demand do
    if not Self.Allow.Contains(S) and not Self.Demand.Contains(S) then
      Exit(false);
  for S in Self.Deny do
    if AContext.Allow.Contains(S) or AContext.Demand.Contains(S) then
      Exit(false);
  for S in AContext.Deny do
    if Self.Allow.Contains(S) or Self.Demand.Contains(S) then
      Exit(false);
  Result := true;
end;

procedure TContext.Validate;
var
  S: String;
begin
  for S in Deny do
    if Demand.Contains(S) or Allow.Contains(S) then
      raise Exception.Create('DENY entry contains ' + S + ' but the context itself ALLOWs or DEMANDs it');
end;

constructor TContext.Create;
begin
  Allow := TStringList.Create;
  Demand := TStringList.Create;
  Deny := TStringList.Create;
end;
destructor TContext.Destroy;
begin
  Allow.Free;
  Demand.Free;
  Deny.Free;
  inherited;
end;

end.

