unit GameBackgrounds;

{$mode objfpc}{$H+}

interface

uses
  CastleImages;

var
  MenuBackground: TCastleImage;
  SubMenuBackground: TCastleImage;

procedure LoadBackgrounds;

implementation

procedure LoadBackgrounds;
begin
  MenuBackground := LoadImage('castle-data:/images/galaxy/space-nebula-1401467530BZx_CC0_by_Dawn_Hudson.jpg');
  SubMenuBackground := LoadImage('castle-data:/images/galaxy/tarantula-nebula_CC0_by_Jean_Beaufort.jpg');
end;

finalization
  MenuBackground.Free;
  SubMenuBackground.Free;

end.

