# Without Form and Void

~~Might be an entry to **Mini Jam 67: Void** https://itch.io/jam/mini-jam-67-void~~

Obviously (who might have thought? ;)) a failed game jam entry.

## About the game

A small roguelike horror exploration game with resources management.

## Installation

### Windows

No installation required - just extract the game into one folder and play.

### Linux

No installation required - just extract the game into one folder and play. Most likely you'll also need to set "executable" flag for the binary (`chmod +x Void` or through file properties in file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

### Android

Download and install the APK.

## Credits

(coming soon)
